# gps-track-mgr

Сервис передает данные по HTTP/2, адрес 167.172.99.203 порт 5002 из БД PostgreSQL 9.5

Сервис написан на C++ 11 с использованием фреймворка GRPC

Средствами сборки (autoconfig/automake для C++ и Gradle для Java (Android)) компилятором protoc с описания RPC метолв и сообщений
Protobuf v 3 генерируется код сервера и клиента для сериализации объектов.

Объектная модель описывается на языке Protobuf 3

Мобильный клиент написан на Java и выполняет RPC запросы.


## Build

using clang:

Download protoc's plugin grpc-web from https://github.com/grpc/grpc-web/releases into /usr/local/bin/protoc-gen-grpc-web


```
./configure CC=clang CXX=clang++
```

## Prerequisites

Check db connection:

```
psql -h localhost -U gpstracker -d gpstracker -c "SELECT * FROM track"
```

Set up administrator

```
INSERT INTO watcher (rights, name, login, password) VALUES(1, 'NAME', 'LOGIN', 'PASSWORD');
```

## Dependencies

- libglog.so.0
- libgflags.so.2.2
- libunwind.so.8

```
scp /usr/local/lib/libglog.so.0 root@www.itctrack.ru:/usr/local/lib/
```

## config file

~/.gps-track-mgr

Lines:

1. Postgres connection info
2. service address
3. login (optional)
4. password (optional)
5. allow user to add device himself (true, false)

Login/password intened for gps-track-cli

Example:

```
dbname=gpstracker user=gpstracker password=PASSWORD
0.0.0.0:5002
```


./gps-track-mgr

./gps-track-mgr -d


## Command line options

Parameters:

 - -d run as daemon
 - -v, -vv, -vvv set verbosity level

## CLI client

gps-track-cli

Commands:

- get get device and last track
- set update device
- add add device
- rm delete device
- ls list of devices
- devices upload devices from the JSON file
- tracks upload tracks from the JSON file
- notify listen fo new tracks

With -o user option:

- get get user
- set update user
- add add user
- rm delete user
- ls list of users

## Notifications

### CLI tools

- js2sheet Send JSON to the Google Sheet
- wpn Send web push notification registered users

### Scripts

- push-web
- push-email
- push-sheet

- naviset2sheet.sh

Run:

```
 nohup ./push-sheet.sh &
 nohup ./push-email.sh &
 nohup ./push-sheet.sh &
```

### Config file ~/.gps-track-mgr

Same as gps-track-mgr.

vi ~/.gps-track-mgr

```
dbname=gpstracker user=gpstracker password=DB_PASSWORD
0.0.0.0:5002
itc
PASSWORD
true
```

Config file lines:

- PostgreSQL connection string with db user and password
- Listen interface and port. 0.0.0.0 means all interfaces
- User login
- User password
- t or f Allow user to add device. Not used.

### Get device and last track

```
./gps-track-cli get --id 87077068744
./gps-track-cli get --id 87077070344

```

### Change device

```
./gps-track-cli set --id 87077068744 --imei "352887077068744" --name "Синий" --color "#9f547f" --pwd "password" --tag 0
./gps-track-cli set --id 87077070344 --imei "352887077070344" --name "Желтый" --color "#3f147f" --pwd "password" --tag 0
```

After password has been set (or changed) provide old password with -p option
```
./gps-track-cli set --id 87077068744 --imei "352887077068744" --name "Синий" --color "#9f547f" --pwd "password" --tag 0 -p oldpassword
./gps-track-cli set --id 87077070344 --imei "352887077070344" --name "Желтый" --color "#3f147f" --pwd "password" --tag 0 -p oldpassword
```

### Remove device

Only administrator can do it
```
./gps-track-cli rm --id 87077068744
```

### List all devices

Only administrator can do it
```
./gps-track-cli ls
{"id":"87077070344","name":"Test 87077070344","color":"#9400d3","imei":"352887077070344"}
{"id":"87077069338","name":"Test 08077069338","color":"#4b0082","imei":"352887077069338"}
{"id":"87077068884","name":"Test 352887077068884","color":"#adff2f","imei":"352887077068884"}
{"id":"87077068744","name":"Test 087077068744","color":"#ffd700","imei":"352887077068744"}

```

or override service's address using -a option:

```
./gps-track-cli -a 167.172.99.203:5002 ls
```
or

```
./gps-track-cli -a 167.172.99.203:5002 ls
```

### Listen for new tracks

```
./gps-track-cli n
{"id":"40","gpstime":"1575607823","lat":62.0148,"lon":129.436,"deviceid":"87077070344"}
```

### List watchers

User must have rights 1.

```
./gps-track-cli -o user ls
{"id":"1","start":"1582280956","rights":1,"login":"root","password":"******"}
{"id":"2","start":"1582281943","rights":1,"login":"itc","password":"******"}
{"id":"3","start":"1585881588","status":1,"login":"user","password":"******","tag":2}
```

### Add a new watcher

User must have rights 1.

```
./gps-track-cli -o user add -L login -r 1 -s 1 -t 2 -W password
```

### Delete watcher

```
./gps-track-cli -o user rm -i 3
```

## gps-track-cli error list

Error 14: Unrecoverable data loss or corruption

- Check service host:port (config file 1st line, -a option)
- start up gps-track-mgr

## Tests

## Config

~/.gps-track-mgr

/usr/share/nginx/html

## Test notification

psql -h localhost -U gpstracker -d gpstracker -c "INSERT INTO track(deviceid, gpstime, lat, lon) VALUES(87077070344, 1575607823, 62.0148, 129.436);"
INSERT 0 1


## Mail settings

https://umonkey.net/postfix/

```
sudo apt install mailutils
sudo vi /etc/postfix/sasl_passwd
sudo vi /etc/postfix/main.cf
vi /etc/postfix/cacert.pem
sudo postmap /etc/postfix/sasl_passwd
cat /etc/ssl/certs/Thawte_Premium_Server_CA.pem | sudo tee -a /etc/postfix/cacert.pem
cat /etc/ssl/certs/postfix restartq | sudo tee -a /etc/postfix/cacert.pem
service postfix restart
echo 1.js| mail -s "Пакет www.itcrack.ru" andrei.i.ivanov@gmail.com
```
