#include <arpa/inet.h>

/**
 * @see https://stackoverflow.com/questions/16375340/c-htonll-and-back
 */
#define htonll(x) ((((uint64_t)htonl(x)) << 32) + htonl((x) >> 32))
#define ntohll(x) ((((uint64_t)ntohl(x)) << 32) + ntohl((x) >> 32))

#ifdef WIN32
#define	SYSLOG(msg) {}
OPENSYSLOG() {  }
CLOSESYSLOG() {  }
#else
#include <syslog.h>
#include <sstream>
#define	SYSLOG(msg) { syslog (LOG_ALERT, "%s", msg); }
#define OPENSYSLOG() { setlogmask (LOG_UPTO(LOG_NOTICE)); openlog("gpstrackmgr", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL1); }
#define CLOSESYSLOG() closelog();
#endif
