#include "packet2json.h"
#include "iridiummessage.h"

static std::string parseNaviset
(
	const std::string &value
)
{
	int tz = 0;
	TIridiumMessageBuild m(tz, value.c_str(), value.size());
	if (!m.valid())
		return "";
  return m.toJson();
}

std::string parsePacket
(
	const std::string &value
)
{
  return parseNaviset(value);
}
