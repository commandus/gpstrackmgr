/* config.h.  Generated from config.h.in by configure.  */
/* config.h.in.  Generated from configure.ac by autoheader.  */

/* Backend: PostgreSQL */
#define DB_POSTGRES 1

/* Define to 1 if you have the <dlfcn.h> header file. */
#define HAVE_DLFCN_H 1

/* Define to 1 if you have the <inttypes.h> header file. */
#define HAVE_INTTYPES_H 1

/* Define to 1 if you have the `pq' library (-lpq). */
#define HAVE_LIBPQ 1

/* Define to 1 if you have the <libpq-fe.h> header file. */
/* #undef HAVE_LIBPQ_FE_H */

/* Define to 1 if you have the <memory.h> header file. */
#define HAVE_MEMORY_H 1

/* Define to 1 if you have the <stdint.h> header file. */
#define HAVE_STDINT_H 1

/* Define to 1 if you have the <stdlib.h> header file. */
#define HAVE_STDLIB_H 1

/* Define to 1 if you have the <strings.h> header file. */
#define HAVE_STRINGS_H 1

/* Define to 1 if you have the <string.h> header file. */
#define HAVE_STRING_H 1

/* Define to 1 if you have the <sys/stat.h> header file. */
#define HAVE_SYS_STAT_H 1

/* Define to 1 if you have the <sys/types.h> header file. */
#define HAVE_SYS_TYPES_H 1

/* Define to 1 if you have the <unistd.h> header file. */
#define HAVE_UNISTD_H 1

/* Define to 1 if you have the </usr/include/pgsql/libpq-fe.h> header file. */
/* #undef HAVE__USR_INCLUDE_PGSQL_LIBPQ_FE_H */

/* Define to 1 if you have the </usr/include/postgresql/libpq-fe.h> header
   file. */
#define HAVE__USR_INCLUDE_POSTGRESQL_LIBPQ_FE_H 1

/* Define to 1 if you have the </usr/local/include/pgsql/libpq-fe.h> header
   file. */
/* #undef HAVE__USR_LOCAL_INCLUDE_PGSQL_LIBPQ_FE_H */

/* Define to 1 if you have the </usr/pgsql-9.4/include/libpq-fe.h> header
   file. */
/* #undef HAVE__USR_PGSQL_9_4_INCLUDE_LIBPQ_FE_H */

/* Define to 1 if you have the </usr/pgsql-9.5/include/libpq-fe.h> header
   file. */
/* #undef HAVE__USR_PGSQL_9_5_INCLUDE_LIBPQ_FE_H */

/* Define to the sub-directory where libtool stores uninstalled libraries. */
#define LT_OBJDIR ".libs/"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "andrei.i.ivanov@commandus.com"

/* Define to the full name of this package. */
#define PACKAGE_NAME "gps-track-mgr"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "gps-track-mgr 0.1"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "gps-track-mgr"

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION "0.1"

/* Define to 1 if you have the ANSI C header files. */
#define STDC_HEADERS 1
