#ifndef PG_LISTENER_H_
#define PG_LISTENER_H_	1

#include <thread>
#include <vector>
#include <grpcpp/grpcpp.h>
#include <grpcpp/alarm.h>
#include <libpq-fe.h>

#include "gpstrack.grpc.pb.h"

class GpsTrackMgrServiceImpl;
class ListenTrackData;

class ListenPg {
private:
  bool stopped;
  // Postgres
  std::string conninfo;
  PGconn *conn;
  std::vector <ListenTrackData*> listenResponders;
  std::thread *listenerThread;
  GpsTrackMgrServiceImpl *trackService;

  bool getTrackFromNotify(gpstracker::Track *retval, PGnotify *notify);
  void notifyClients(const std::vector<gpstracker::Track> &values);
  int listenNotifications();
  int handlePgRead();
public:
  ListenPg(GpsTrackMgrServiceImpl *owner);
  virtual ~ListenPg();
  void start(const std::string &conninfo);
  void stop();
  void done();
  bool checkConnStatus();
  void printListenResponders();
  void put(ListenTrackData *value);
  bool rm(ListenTrackData *value);
  void enqueue(const std::vector<gpstracker::Track> &values);
};

#endif
