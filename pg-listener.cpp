#include "pg-listener.h"
#include <sstream>
#include <grpcpp/grpcpp.h>
#include <grpc/support/log.h>
#include "errlist.h"
#include "platform.h"

#include <unistd.h>
#include <sys/un.h>
#include <fcntl.h>

#include "service-raw.h"

ListenPg::ListenPg(GpsTrackMgrServiceImpl *owner)
  : stopped(true), conn(NULL), listenerThread(NULL), trackService(owner)
{
}

ListenPg::~ListenPg()
{
  stop();
}

bool ListenPg::getTrackFromNotify(
	gpstracker::Track *retval,
	PGnotify *notify
) {
  // parse pre-order
  google::protobuf::util::Status status = google::protobuf::util::JsonStringToMessage(notify->extra, retval);
  if (!status.ok()) {
    return false;
  }
  return true;
}

void ListenPg::notifyClients(const std::vector<gpstracker::Track> &values)
{
  // std::cerr << "notify clients: track " << t.id() << std::endl;
  if (!values.empty())
    trackService->queuingMgr->enqueue(values);
}

void ListenPg::start(
  const std::string &aconninfo
)
{
  conninfo = aconninfo;
  if (!stopped)
    return;
  if (conn)
    return;
  if (listenerThread)
    return;
  stopped = false;
  listenerThread = new std::thread(&ListenPg::listenNotifications, this);
}

void ListenPg::stop()
{
  if (stopped)
    return;
  if (!conn)
    return;
  if (!listenerThread)
    return;
  stopped = true;
  listenerThread->join();
  listenerThread = NULL;
}

bool ListenPg::checkConnStatus() {
  if (PQstatus(conn) == CONNECTION_BAD) {
    conn = PQconnectdb(conninfo.c_str());
    return (PQstatus(conn) == CONNECTION_OK);
  }
  return true;
}

int ListenPg::handlePgRead()
{
  PGnotify *notify;
  PGresult *res;
  
  // read data waiting in buffer
  if (!PQconsumeInput(conn)) {
    return ERR_CODE_PG_CONSUME_INPUT;
  }

  // got query results?
  while ((res = PQgetResult(conn))) {
    if (PQresultStatus(res) != PGRES_COMMAND_OK) {
      PQclear(res);
      return ERR_CODE_PG_RESULT;
    }
  }
  
  std::vector<gpstracker::Track> tracks;
  // check for async notifications
  while ((notify = PQnotifies(conn))) {
    gpstracker::Track t;
    if (getTrackFromNotify(&t, notify))
      tracks.push_back(t);
    PQfreemem(notify);
  }
  notifyClients(tracks);
  return 0;
}

void ListenPg::done()
{
  listenerThread = NULL;
  conn = NULL;
  stopped = true;
}

/**
 * @see https://github.com/revmischa/pgnotify-demos/blob/master/pglisten.c
 */
int ListenPg::listenNotifications()
{
  int connected = 0;
  int connPollReady = 0;

  conn = PQconnectdb(conninfo.c_str());
  
  while (!stopped) {
    // get connection underlying fd
    int dbsocket = PQsocket(conn);
    if (dbsocket < 0) {
      done();
      return ERR_CODE_PG_CONNECTION;
    }

    // initialize select() fd sets
    fd_set rfds, wfds;
    FD_ZERO(&rfds);
    FD_ZERO(&wfds);

    if (!connected) {
      if (connPollReady) {
        // ready to poll for connection status?
        PostgresPollingStatusType pollStatus = PQconnectPoll(conn);
        switch (pollStatus) {
        case PGRES_POLLING_FAILED:
          // connect failed
          done();
          return ERR_CODE_PG_CONNECTION;
        case PGRES_POLLING_WRITING:
          // ready to send data to server
          FD_SET(dbsocket, &wfds);
          break;
        case PGRES_POLLING_ACTIVE:
          break;
        case PGRES_POLLING_READING:
          // ready to read back from server
          FD_SET(dbsocket, &rfds);
          break;
        case PGRES_POLLING_OK:
          // we are now connected and done polling the connection state
          connected = 1;
          if (PQsendQuery(conn, "LISTEN \"track\"") != 1) {
            done();
            return ERR_CODE_PG_LISTEN;
          }
          break;
        }
      } else {
        // wait for sock fd to become writable
        FD_SET(dbsocket, &wfds);
      }
    }

    if (connected) {
      // select on connection becoming readable
      FD_SET(dbsocket, &rfds);
    }

    struct timeval timeout;
    timeout.tv_sec = 30;
    timeout.tv_usec = 0;
    // hang out until there is stuff to read or write
    int retval = select(dbsocket + 1, &rfds, &wfds, NULL, &timeout);
    switch (retval) {
    case -1:
      if (!stopped) {
        done();
        return ERR_CODE_SELECT;
      }
      stopped = true;
      break;
    case 0:
      // std::cerr << ERR_PG_TIMEOUT << std::endl;
      break;
    default:
      connPollReady = 1;
      if (!connected)
        break;

      // this is always going to be true, but you'll want to
      // check it if you are selecting on more than one fd
      if (FD_ISSET(dbsocket, &rfds)) {
        // handle
        handlePgRead();
      }
      break;
    }
  }

  if (conn) {
    PQfinish(conn);
  }

  done();
  return TRACKER_OK;
}

void ListenPg::printListenResponders()
{
  std::cerr << "Responders:" << std::endl;
  for (auto it(listenResponders.begin()); it != listenResponders.end(); ++it) {
    std::cerr << *it << std::endl;
  }
}

void ListenPg::put(ListenTrackData *value)
{
  listenResponders.push_back(value);
}

bool ListenPg::rm(ListenTrackData *value)
{
  for (std::vector <ListenTrackData*>::iterator it(listenResponders.begin()); it != listenResponders.end(); ++it) {
    if (*it == value) {
      listenResponders.erase(it);
      return true;
    }
  }
  return false;
}

void ListenPg::enqueue(
  const std::vector<gpstracker::Track> &values
) {
  for (std::vector<ListenTrackData*>::iterator it(listenResponders.begin()); it != listenResponders.end(); ++it) {
    // std::cerr << "ListenPg::enqueue " << std::endl;
    (*it)->enqueue(values);
  }
}
