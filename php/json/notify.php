<?php
  require './vendor/autoload.php';
  use Minishlink\WebPush\WebPush;
  use Minishlink\WebPush\Subscription;

  function mkMessage($title, $body, $icon, $link) {
    $U = "https://gs.commandus.com/assets/icons/";
    $r = array();
    $r['notification'] = array();
    $r['notification']['title'] = $title;
    $r['notification']['body'] = $body;
    $r['notification']['icon'] = $icon;
    $r['notification']['badge'] = $U . "icon-32x32.png";
    $r['notification']['image'] = $U . "logo-512x256.png";
    $r['notification']['click_action'] = $link;
    $r['notification']['requireInteraction'] = true;
    // $r['notification']['renotify'] = true;
    $r['notification']['actions'] = array();
    $action1 = array();
    $action1['action'] = 'open';
    $action1['title'] = 'Открыть';
    $r['notification']['actions'][0] = $action1;
    $action2 = array();
    $action2['action'] = 'close';
    $action2['title'] = 'Закрыть';

    $r['notification']['actions'][0] = $action1;
    $r['notification']['actions'][1] = $action2;

    return json_encode($r, JSON_NUMERIC_CHECK);
}

  function pushNotification($endpoint, $publicKey, $authToken, $msg) {
    $auth = [
        'VAPID' => [
            'subject' => 'mailto:support@commandus.com',
            'publicKey' => 'BMWbr4dF-V8-fdxch8ZaWrGMgvnF_gJ4sQAGJ4ByUKs7hDQmaixBuJkKvoXi6RYYL2DtOtU7Ktig2-IfowSsb4A',
            'privateKey' => '_93Jy3cT0SRuUA1B9-D8X_zfszukGUMjIcO5y44rqCk'
        ],
    ];
    $webPush = new WebPush($auth);
    $sent = $webPush->sendNotification(
        Subscription::create([
            'endpoint' => $endpoint,
            'publicKey' => $publicKey,
            'authToken' => $authToken,
            'contentEncoding' => 'aes128gcm'
        ]),
        mkMessage("Горснаб", $msg, "https://gorsnab.gorsnab-ykt.ru/favicon.ico", "https://gorsnab.gorsnab-ykt.ru/"));
    $ri = array();
    foreach ($webPush->flush() as $r) {
        if ($r->isSuccess()) {
            $ri['r'] = true;
        } else {
            $ri['r'] = false;
            $ri['reason'] = $r->getReason();
        }
    }
    return $ri;
    }


    $endpoint="https://fcm.googleapis.com/wp/feYyPOQOMDY:APA91bFka9i0KPYz7UM8CNYiJ3AtZnhjdasPlGoXb6FbqU0rcIbGeon2fThkRfB2KYQLlQ6ed6MVLwcT4L1-ROcA41bC38DgncBpFd24Cv4DeHMK3VURnx4_3ADPijAdgEJ1VwPZVol_";
    $p256dh="BMWbr4dF-V8-fdxch8ZaWrGMgvnF_gJ4sQAGJ4ByUKs7hDQmaixBuJkKvoXi6RYYL2DtOtU7Ktig2-IfowSsb4A";
    $auth="Wyg-77tbRl79qfDHL6EWGQ";
    $msg="Message body";
    $r = pushNotification($endpoint, $p256dh, $auth, $msg);
    print_r($r);
