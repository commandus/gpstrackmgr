#!/bin/bash
P=/home/andrei/src/gpstracklistener
EMAILS=itctrack@mail.ru
while read -r line
do
    ATT=$(echo $line | jq .)
    id=$(echo $line | jq '.msg.track.id|tonumber') 
    lat=$(echo $line | jq '.msg.track.lat') 
    lon=$(echo $line | jq '.msg.track.lon') 
    deviceid=$(echo $line | jq '.msg.track.deviceid|tonumber') 
    t=$(echo $line | jq '.msg.track.gpstime|tonumber') 
    tt=$(date -d @$t)
    
    raw=$(echo $line | jq '.msg.rawdata.hex') 
    src=$(echo $line | jq '.msg.rawdata.srcaddr') 
    port=$(echo $line | jq '.msg.rawdata.srcport') 
    linkgoogle="https://www.google.com/maps/search/?api=1&query=$lat,$lon"
    linkyandex="https://yandex.ru/maps/?pt=$lat,$lon&z=18&l=sat,skl"
    ts=$(date)
    msg="Получен пакет данных от $src:$port, запись $id\n\nОшейник $deviceid\n$tt ($t)\n($lat, $lon)\
    \n$map\
    \nПакет $raw\n\nПодробнее:\n$ATT\n\
    \n$linkgoogle\
    \n$linkyandex\
    \n\nОтправлено $ts\n\nЭто автоматически сгенерированное сообщение.\nНе отвечайте на него.\n"
    echo -e $msg | mail -s "www.itcrack.ru $deviceid" $EMAILS
done < <($P/gps-track-cli n -l itc -P Rfhnjirf19)
