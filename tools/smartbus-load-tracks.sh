#!/bin/sh
# curl -A "okhttp_4da3c038c65ca1d2_88fae687" -H "Content-MD5-Hash: `./datehash.sh`" -X GET -L "http://apitest2.bus62.ru:8080/getVehicleAnimations.php?city=yakutsk&rids=29&curk=0" > 1.js
v=tools/1.js
DS=$(cat $v | jq -L. -Ltools 'include "cvrt";.[] | {id:(.id[20:32]|to_i(16)), imei:(.gos_num), name:(.rnum), tag:(.gos_num|gsub("[.А-Яа-яA-Za-z]";"")|tonumber)}' | jq -s '')
TS=$(cat $v | jq -L. -Ltools 'include "cvrt";.[] | {id:(.id[20:32]|to_i(16)), lon:(.lng|tonumber/1000000), lat:(.lat|tonumber/1000000), direction:(.dir|tonumber), gpstime:(.time|sub(" ";"T") +"Z"|fromdate)}' |  jq -s '')
echo $DS | ./gps-track-cli json-devices
echo $TS | ./gps-track-cli json-tracks

