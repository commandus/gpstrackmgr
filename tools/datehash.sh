#!/bin/sh
#
# "06:41:11 2020-01-02             "
# B5523D6C8CDF3BD53E27C53C822B88ABF7DB24C080D57A7BE95CD75D34A80E14
# echo "06:41:11 2020-01-02             " | tr -d '\n' | openssl enc -aes-128-cbc -nopad -iv "30424151454641415342395443423867" -K "31326a49614350304b386e4b3078484e" | od -A n -v -t x1 | tr -d ' \n'
#
date -u '+%S:%M:%H %Y-%m-%d             ' | tr -d '\n' | openssl enc -aes-128-cbc -nopad -iv "30424151454641415342395443423867" -K "31326a49614350304b386e4b3078484e" | od -A n -v -t x1 | tr -d ' \n'
